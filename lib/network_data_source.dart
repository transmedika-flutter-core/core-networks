import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/google/place/place_detail_response.dart';
import 'package:core_model/network/google/place/place_response.dart';
import 'package:core_model/network/param/forgot_param.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/country_code.dart';
import 'package:core_model/network/response/district.dart';
import 'package:core_model/network/response/location.dart';
import 'package:core_model/network/response/profile.dart';
import 'package:core_model/network/response/province.dart';
import 'package:core_model/network/response/regency.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/network/response/sub_district.dart';
import 'package:dio/dio.dart';

import 'googleapi/map_google_api_service.dart';
import 'myapi/my_api_service.dart';
import 'network_data_source_service.dart';


class NetworkDataSource implements NetworkDataSourceService{
  final MyApiService myApiService;
  final MapGoogleApiService mapGgoogleApiService;

  NetworkDataSource({required this.myApiService, required this.mapGgoogleApiService});

  @override
  Future<BaseResponse<SignIn>> postSignIn(SignInParam param) => myApiService.postSignIn(param.tokenId, param);

  @override
  Future<BaseResponse<SignIn>> postSignUp(SignUpParam param) => myApiService.postSignUp(param);

  @override
  Future<BaseResponse<dynamic>> postForgot(ForgotParam param) => myApiService.postFogot(param);

  @override
  Future<BaseResponse<List<Province>>> getProvincies() => myApiService.getProvincies();

  @override
  Future<BaseResponse<List<Regency>>> getRegenciesByProvinceId(int provinceId) => myApiService.getRegenciesByProvinceId(provinceId);

  @override
  Future<BaseResponse<Profile<dynamic>>> getProfile(CancelToken? cancelToken) => myApiService.getProfile(cancelToken);

  @override
  Future<BaseResponse<List<District>>> getDistrictsByRegencyId(int regencyId) => myApiService.getDistrictsByRegencyId(regencyId);

  @override
  Future<BaseResponse<List<SubDistrict>>> getSubDistrictsByDistrictId(int districtId) => myApiService.getSubDistrictsByDistrictId(districtId);

  @override
  Future<PlaceDetailResponse> getPlaceDetail(String placeId, String? fields) {
    return mapGgoogleApiService.getPlaceDetail(placeId, fields ?? 'address_component,geometry');
  }

  @override
  Future<PlaceResponse> getPlaces(String input, String? types, String? components) {
    return mapGgoogleApiService.getPlaces(input, types ?? '', components ?? 'country:id');
  }

  @override
  Future<BaseResponse<List<CountryCode>>> getCountries(String? search) {
    return myApiService.getCountries(search);
  }

  @override
  Future<BaseResponse<List<LocationResponse>>> getLocationBySubDistrict(
          String search) =>
      myApiService.getLocationBySubDistrict(search);
}