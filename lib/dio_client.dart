import 'package:core_model/network/response/signin.dart';
import 'package:core_shared_preferences/shared_preferences_data_source_service.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:core_constants/constants.dart';
import 'package:core_model/network/exception/network_error_code.dart';
import 'dart:io' show Platform;

class NetworkInterceptor extends Interceptor {
  final SharedPreferencesDataSourceService preferencesDataSourceService;

  NetworkInterceptor({required this.preferencesDataSourceService});

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers["Content-Type"] = "application/json";
    options.headers["Accept"] = "application/json";

    if (kIsWeb) {
      options.headers["Access-Control-Allow-Origin"] = "*";
      options.headers["Access-Control-Allow-Methods"] = "POST,DELETE";
      options.headers["Access-Control-Allow-Headers"] = "X-Requested-With";
    }

    options.headers["is-mobile"] = "true";

    List<String> urlWithoutToken = ['login'];
    if (!urlWithoutToken.contains(options.path)) {
      SignIn? signIn = await preferencesDataSourceService.getSession();
      options.headers["Authorization"] = "Bearer ${signIn?.token ?? signIn?.authtoken ?? signIn?.xAuthtoken}";
    }

    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String appName = packageInfo.appName;
    String packageName = packageInfo.packageName;
    String version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;

    options.headers["App-Name"] = appName;
    options.headers["Package-Name"] = packageName;
    options.headers["App-Version"] = version;
    options.headers["Build-Number"] = buildNumber;

    try {
      final String deviceOS;
      final String deviceBrand;
      final String deviceModel;
      DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
      if (defaultTargetPlatform == TargetPlatform.android) {
        final androidInfo = await deviceInfo.androidInfo;
        deviceOS = "Android";
        deviceBrand = androidInfo.brand;
        deviceModel = androidInfo.model;
      } else if (defaultTargetPlatform == TargetPlatform.iOS) {
        final iOSInfo = await deviceInfo.iosInfo;
        deviceOS = "iOS";
        deviceBrand = "Apple";
        deviceModel = iOSInfo.model;
      } else {
        throw Exception("Undifined target platform");
      }

      options.headers["Device-Os"] = deviceOS;
      options.headers["device-brand"] = deviceBrand;
      options.headers["device-model"] = deviceModel;
    } catch (e) {
      debugPrint("Error platform info: $e");
    }

    final localizationSetting =
        await preferencesDataSourceService.getLocalizationSetting();
    options.headers["X-Localization"] =
        localizationSetting?.languageCode ?? Constants.defaultLocale;

    return handler.next(options);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    if (err.response?.statusCode == NetworkErrorCode.unauthorized) {
      preferencesDataSourceService.removeSession();
    }
    return handler.next(err);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    if (response.statusCode == NetworkErrorCode.unauthorized) {
      preferencesDataSourceService.removeSession();
    }
    return handler.next(response);
  }
}

class MapGoogleNetworkInterceptor extends Interceptor {
  MapGoogleNetworkInterceptor({
    required this.sessiontoken,
    required this.key,
    this.iosBundleId,
    this.androidPackage,
    this.androidCert,
  });
  final String sessiontoken;
  final String key;
  final String? iosBundleId;
  final String? androidPackage;
  final String? androidCert;

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    options.queryParameters["sessiontoken"] = sessiontoken;
    options.queryParameters["key"] = key;
    if (Platform.isAndroid) {
      options.headers["X-Android-Package"] = androidPackage;
      options.headers["X-Android-Cert"] = androidCert;
    } else if (Platform.isIOS) {
      options.headers["X-Ios-Bundle-Identifier"] = iosBundleId;
    }

    return handler.next(options);
  }

  @override
  void onError(DioException err, ErrorInterceptorHandler handler) {
    return handler.next(err);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    return handler.next(response);
  }
}
