import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/google/place/place_detail_response.dart';
import 'package:core_model/network/google/place/place_response.dart';
import 'package:core_model/network/param/forgot_param.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/country_code.dart';
import 'package:core_model/network/response/district.dart';
import 'package:core_model/network/response/location.dart';
import 'package:core_model/network/response/profile.dart';
import 'package:core_model/network/response/province.dart';
import 'package:core_model/network/response/regency.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/network/response/sub_district.dart';
import 'package:dio/dio.dart';

abstract class NetworkDataSourceService{
  Future<BaseResponse<SignIn>> postSignIn(SignInParam param);
  Future<BaseResponse<SignIn>> postSignUp(SignUpParam param);
  Future<BaseResponse<dynamic>> postForgot(ForgotParam param);
  Future<BaseResponse<List<Province>>> getProvincies();
  Future<BaseResponse<List<Regency>>> getRegenciesByProvinceId(int provinceId);
  Future<BaseResponse<Profile<dynamic>>> getProfile(CancelToken? cancelToken);
  Future<BaseResponse<List<District>>> getDistrictsByRegencyId(int regencyId);
  Future<BaseResponse<List<SubDistrict>>> getSubDistrictsByDistrictId(int districtId);
  Future<PlaceResponse> getPlaces(String input, String? types, String? components);
  Future<PlaceDetailResponse> getPlaceDetail(String placeId,String? fields);
  Future<BaseResponse<List<CountryCode>>> getCountries(String? search);
  Future<BaseResponse<List<LocationResponse>>> getLocationBySubDistrict(String search);
}