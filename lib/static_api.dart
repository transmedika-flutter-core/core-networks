class MyApiStatic {
  static const String prodUrl = "http://dhpos.primateknologikreatif.com/api/";
  static const String devUrl = "https://api-idi.transmedic.co.id/api/v1/";
  static const String baseUrl = devUrl;
  static const String signInPath = "auth/login";
  static const String signUpPath = "auth/register";
  static const String forgotPath = "auth/forgot-password";
  static const String provincesPath = "provinces";
  static const String regenciesPath = "regencies-province/{province_id}";
  static const String profilePath = "profile";
  static const String districtsPath = "district-regencies/{regency_id}";
  static const String subDistrictsPath = "sub-district/{district_id}";
  static const String countries = "countries";
  static const String locations = "locations";
}

class MapGoogleApiStatic {
  static const String baseUrl = "https://maps.googleapis.com/maps/api/";
  static const String placesPath = "place/autocomplete/json";
  static const String placeDetailPath = "place/details/json";
  static const String reverseGeocodePath = "geocode/json";
}
