
import 'package:core_model/network/google/place/place_detail_response.dart';
import 'package:core_model/network/google/place/place_response.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

import '../static_api.dart';

part 'map_google_api_service.g.dart';

@RestApi(baseUrl: MapGoogleApiStatic.baseUrl)
abstract class MapGoogleApiService {
  factory MapGoogleApiService(Dio dio, {String baseUrl}) = _MapGoogleApiService;

  @GET(MapGoogleApiStatic.placesPath)
  Future<PlaceResponse> getPlaces(
    @Query("input") String input,
    @Query("types") String types,
    @Query("components") String components
  );

  @GET(MapGoogleApiStatic.placeDetailPath)
  Future<PlaceDetailResponse> getPlaceDetail(
    @Query("place_id") String placeId,
    @Query("fields") String fields,
  );

  @GET(MapGoogleApiStatic.reverseGeocodePath)
  Future<PlaceDetailResponse> getAddressFromGeoPoint(
      @Query("latlng") String latlng
  );
}