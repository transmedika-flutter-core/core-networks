import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/param/forgot_param.dart';
import 'package:core_model/network/param/signin_param.dart';
import 'package:core_model/network/param/signup_param.dart';
import 'package:core_model/network/response/country_code.dart';
import 'package:core_model/network/response/district.dart';
import 'package:core_model/network/response/location.dart';
import 'package:core_model/network/response/profile.dart';
import 'package:core_model/network/response/province.dart';
import 'package:core_model/network/response/regency.dart';
import 'package:core_model/network/response/signin.dart';
import 'package:core_model/network/response/sub_district.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';

import '../static_api.dart';

part 'my_api_service.g.dart';

@RestApi(baseUrl: MyApiStatic.baseUrl)
abstract class MyApiService {
  factory MyApiService(Dio dio, {String baseUrl}) = _MyApiService;

  @POST(MyApiStatic.signInPath)
  Future<BaseResponse<SignIn>> postSignIn(
      @Header("device-id") String? deviceId,
      @Body() SignInParam param,
  );

  @POST(MyApiStatic.signUpPath)
  Future<BaseResponse<SignIn>> postSignUp(
      @Body() SignUpParam param
  );

  @POST(MyApiStatic.forgotPath)
  Future<BaseResponse<dynamic>> postFogot(
      @Body() ForgotParam param
  );

  @GET(MyApiStatic.provincesPath)
  Future<BaseResponse<List<Province>>> getProvincies();

  @GET(MyApiStatic.regenciesPath)
  Future<BaseResponse<List<Regency>>> getRegenciesByProvinceId(
      @Path("province_id") int provinceId
  );

  @GET(MyApiStatic.profilePath)
  Future<BaseResponse<Profile<dynamic>>> getProfile(
      @CancelRequest() CancelToken? cancelToken);

  @GET(MyApiStatic.districtsPath)
  Future<BaseResponse<List<District>>> getDistrictsByRegencyId(
      @Path("regency_id") int regencyId
  );

  @GET(MyApiStatic.subDistrictsPath)
  Future<BaseResponse<List<SubDistrict>>> getSubDistrictsByDistrictId(
      @Path("district_id") int districtId
  );

  @GET(MyApiStatic.countries)
  Future<BaseResponse<List<CountryCode>>> getCountries(
      @Query("search") String? search
  );

  @GET(MyApiStatic.locations)
  Future<BaseResponse<List<LocationResponse>>> getLocationBySubDistrict(
      @Query("search") String search);
}
