import 'package:core_model/network/base/base_result.dart';

abstract class UseCaseService<Type, Params> {
  Future<Result<Type>> call(Params param);

}
