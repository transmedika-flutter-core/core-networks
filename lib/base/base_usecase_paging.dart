import 'package:core_model/network/base/base_paging_param.dart';
import 'package:core_network/base/base_usecase_service.dart';

abstract class UseCasePaging<T, PARAM extends PagingParam> implements UseCaseService<List<T>, PARAM> {}