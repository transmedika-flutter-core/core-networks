import 'package:core_model/helpers/exception_message.dart';
import 'package:core_model/network/base/base_result.dart';
import 'package:core_model/network/base/baseresponse.dart';
import 'package:core_model/network/base/paging.dart';
import 'package:core_model/network/exception/list_exception.dart';
import 'package:core_model/network/exception/network_exception.dart';
import 'package:core_model/network/exception/paging_exception.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class BaseUseCase{
  Future<Result<T>> getBaseResponseData<T>(Future<BaseResponse<T>?> api) async {
    try {
      BaseResponse? resp = await api;
      final data = resp?.data;
      if (data != null) {
        if (data is List<dynamic>) {
          final dataList = data;
          if (dataList.isEmpty) {
            return ResultError(error: EmptyListException("Data list is empty"));
          } else {
            return ResultSuccess(data: data as T);
          }
        } else if (data is Paging) {
          final total = data.total;
          if (total > 0) {
            return ResultSuccess(data: data.data);
          } else {
            return ResultError(
                error: EmptyPagingException("Data paging is empty"));
          }
        } else {
          debugPrint("Base Repository onResponse success: $data");
          return ResultSuccess(data: data);
        }
      } else {
        return ResultError(
            error: NetworkException(
              resp?.message,
            ));
      }
    } on DioException catch (ex) {
      debugPrint("Base Repository getBaseResponseData DioException: $ex");
      debugPrint(
          "Base Repository getBaseResponseData DioException: ${ex.message}");
      var networkEx = _getErrorFromDio(ex);
      return ResultError(error: networkEx);
    } catch (ex) {
      debugPrint("Base Repository getBaseResponseData ex: $ex");
      return ResultError(error: Exception(ex));
    }
  }

  Future<Result<U>> getBaseResponseDataTransform<T, U>(
      Future<BaseResponse<T>?> api,
      {required U Function(T resp) onTransform,
        U? Function()? onResultNull}) async {
    try {
      BaseResponse<T>? resp = await api;
      final data = resp?.data;
      if (data != null) {
        if (data is List<dynamic>) {
          final dataList = data;
          if (dataList.isEmpty) {
            return ResultError(error: EmptyListException("Data list is empty"));
          } else {
            return ResultSuccess(data: onTransform(data));
          }
        } else if (data is Paging) {
          final total = data.total;
          if (total > 0) {
            return ResultSuccess(data: onTransform(data));
          } else {
            return ResultError(
                error: EmptyPagingException("Data paging is empty"));
          }
        } else {
          return ResultSuccess(data: onTransform(data));
        }
      } else {
        if (onResultNull != null) {
          if (resp?.status == true) {
            return ResultSuccess(data: onResultNull());
          }
        }
        return ResultError(error: NetworkException(resp?.message));
      }
    } on DioException catch (ex) {
      debugPrint("Base Repository Error: $ex");
      var networkEx = _getErrorFromDio(ex);
      return ResultError(error: networkEx);
    } catch (ex) {
      return ResultError(error: Exception(ex));
    }
  }

  Future<Result<U>> getPagingResponseData<T, U>(
      Future<BaseResponse<Paging<T>>?> api,
      {required U Function(T resp) onTransform}) async {
    try {
      BaseResponse<Paging<T>>? resp = await api;
      final data = resp?.data;
      if (data != null) {
        final total = data.total;
        final dataPage = data.data;
        if (total > 0 && dataPage != null) {
          return ResultSuccess(data: onTransform(dataPage));
        } else {
          debugPrint("Masuk empty");
          return ResultError(error: EmptyPagingException("Paging is empty"));
        }
      } else {
        return ResultError(error: NetworkException(resp?.message));
      }
    } on DioException catch (ex) {
      debugPrint("Base Repository DioException catch $ex");
      var networkEx = _getErrorFromDio(ex);
      return ResultError(error: networkEx);
    } on Exception catch (ex) {
      debugPrint("Base Repository Exception catch $ex");
      return ResultError(error: ex);
    } catch (ex) {
      debugPrint(
          "Base Repository getPagingResponseData exception: ${ex.toString()}");
      return ResultError(error: Exception(ex));
    }
  }

  NetworkException _getErrorFromDio(DioException dio) {
    final code = dio.response?.statusCode;
    if (code != null && code.toString().startsWith("4") ||
        code.toString().startsWith("5")) {
      final message = dio.getErrorMessage();
      return NetworkException(message, code: code);
    }
    return NetworkException(null);
  }

  Future<Result<T>> getBaseDummyData<T>(Future<T?> api) async {
    try {
      final data = await api;
      if (data != null) {
        if (data is List<dynamic>) {
          final dataList = data;
          if (dataList.isEmpty) {
            return ResultError(error: EmptyListException("Data list is empty"));
          } else {
            return ResultSuccess(data: data as T);
          }
        } else if (data is Paging) {
          final total = data.total;
          if (total > 0) {
            return ResultSuccess(data: data.data);
          } else {
            return ResultError(
                error: EmptyPagingException("Data paging is empty"));
          }
        } else {
          debugPrint("Base Repository onResponse success: $data");
          return ResultSuccess(data: data);
        }
      } else {
        return ResultError(
            error: Exception("Data is null"));
      }
    } catch (ex) {
      debugPrint("Base Repository getBaseResponseData ex: $ex");
      return ResultError(error: Exception(ex));
    }
  }

  Future<Result<U>> getDummyDataTransform<T, U>(
      Future<T?> api,
      {required U Function(T resp) onTransform,
        U? Function()? onResultNull}) async {
    try {
      final data = await api;
      if (data != null) {
        if (data is List<dynamic>) {
          final dataList = data;
          if (dataList.isEmpty) {
            return ResultError(error: EmptyListException("Data list is empty"));
          } else {
            return ResultSuccess(data: onTransform(data));
          }
        } else if (data is Paging) {
          final total = data.total;
          if (total > 0) {
            return ResultSuccess(data: onTransform(data));
          } else {
            return ResultError(
                error: EmptyPagingException("Data paging is empty"));
          }
        } else {
          return ResultSuccess(data: onTransform(data));
        }
      } else {
        return ResultError(
            error: Exception("Data is null"));
      }
    } catch (ex) {
      return ResultError(error: Exception(ex));
    }
  }
  Future<Result<U>> getPagingDummyData<T, U>(
      Future<Paging<T>?> api,
      {required U Function(T resp) onTransform}) async {
    try {
      Paging<T>? data = await api;
      if (data != null) {
        final total = data.total;
        final dataPage = data.data;
        if (total > 0 && dataPage != null) {
          return ResultSuccess(data: onTransform(dataPage));
        } else {
          debugPrint("Masuk empty");
          return ResultError(error: EmptyPagingException("Paging is empty"));
        }
      } else {
        return ResultError(
            error: Exception("Data is null"));;
      }
    } on Exception catch (ex) {
      debugPrint("Base Repository Exception catch $ex");
      return ResultError(error: ex);
    } catch (ex) {
      debugPrint(
          "Base Repository getPagingResponseData exception: ${ex.toString()}");
      return ResultError(error: Exception(ex));
    }
  }
}